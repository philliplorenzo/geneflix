const Strategy = require('passport-local').Strategy
const members = require('./models/members')

module.exports = passport => {
  passport.use(new Strategy(
    (username, password, cb) => {
      members.findByUsername(username).then((member) => {
        if (!member) {
          return cb(null, false)
        }
        members.isValidPassword2(password, member.password)
          .then(isValid => {
            if (isValid) {
              return cb(null, member)
            }
            return cb(null, false)
          })
      })
        .catch(error => console.error(error))
    }
  ))

  passport.serializeUser((member, cb) => {
    cb(null, member.id)
  })

  passport.deserializeUser((memberId, cb) => {
    members.findById(memberId)
      .then((member) => {
        return cb(null, member)
      })
  })
  return passport
}