const db = require('./db/content')

module.exports = {
  create: db.create,
  getAll: db.getAll,
  destroy: db.destroy
}