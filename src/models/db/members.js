const db = require('./db')
const bcrypt = require('bcrypt')
const saltRounds = 10

const create = (username, email, password) => {
  bcrypt.hash(password, saltRounds).then(hash => {
    return db.query(`
      INSERT INTO member
      (username, email, password)
      VALUES 
      ($1, $2, $3)
      RETURNING *
    ;`,
    [username, email, hash])
    })
    .then(result => console.log)
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.create',
        arguments: arguments
      })
      throw error
    })
  }
  
  const setActiveStatus = (id) => {
    return db.query(`
    UPDATE member SET status=true
    WHERE id=$1
    `, 
    [id])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.create',
        arguments: arguments
      })
      throw error
    })
  }

  const findById = (memberId) => {
    return db.oneOrNone(`
      SELECT * FROM member
      WHERE id=$1
    ;`,
    [memberId])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.findById',
        arguments: arguments
      })
      throw error
    })
  }
  
  const findByEmail = (email) => {
    return db.oneOrNone(`
      SELECT * FROM member 
      WHERE email=$1
    ;`,
    [email])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.findByEmail',
        arguments: arguments
      })
      throw error
    })
  }
  
  const findByUsername = (username) => {
    return db.oneOrNone(`
      SELECT * FROM member 
      WHERE username=$1
    ;`, 
    [username])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.findByUsername',
        arguments: arguments
      })
      throw error
    })
  }
  
  const getUsername = (id) => {
    return db.oneOrNone(`
      SELECT username FROM member 
      WHERE id=$1
    ;`, 
    [id])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.getUsername',
        arguments: arguments
      })
      throw error
    })
  }
  
  const getAll = () => {
    return db.manyOrNone(`
    SELECT * FROM member
    ORDER BY id
    ;`)
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.getUsername',
        arguments: arguments
      })
      throw error
    })
  }
  
  const destroy = (memberId) => {
    return db.query(`
      DELETE FROM 
      member
      WHERE id=$1
    ;`, 
    [memberId])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.destroy',
        arguments: arguments
      })
      throw error
    })
  }


module.exports = {
  create,
  findById,
  findByEmail,
  findByUsername,
  getUsername,
  setActiveStatus,
  getAll,
  destroy
}