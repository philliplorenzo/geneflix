const db = require('./db')
const members = require('./members')

const assignAdminRole = (id) => {
  return db.query(`
    UPDATE member SET role='admin'
    WHERE id=$1
    RETURNING *
  `, [id])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.create',
        arguments: arguments
      })
      throw error
    })
  }
  
  const assignStandardRole = (id) => {
    return db.query(`
    UPDATE member SET role='standard'
    WHERE id=$1
    RETURNING *
    `, [id])
    .catch(error => {
      console.error({
        message: 'Error occurred while executing members.create',
        arguments: arguments
      })
      throw error
    })
  }
  
  const removeAdminRole = (id) => {
    return db.query(`
    UPDATE member SET role='standard'
    WHERE id=$1
    RETURNING *
    `, [id])
      .catch(error => {
        console.error({
          message: 'Error occurred while executing members.create',
          arguments: arguments
        })
        throw error
      })
    }
          
const removeStandardRole = (id) => {
  return db.query(`
  UPDATE member SET role='guest'
  WHERE id=$1
  RETURNING *
  `, [id])
      .catch(error => {
        console.error({
          message: 'Error occurred while executing members.create',
          arguments: arguments
        })
        throw error
      })
}

module.exports = {
  assignAdminRole,
  assignStandardRole,
  removeAdminRole,
  removeStandardRole
}
