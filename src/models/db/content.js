const db = require('./db')
const members = require('./members')

const create = (content) => {
  return db.tx(t => {
    const createPrimContent = t.one(`INSERT INTO content (name, description, release_year, trt, keyword) VALUES ($1, $2, $3, $4, $5) RETURNING id`, [name, description, release_year, trt, keyword])
    const createDirector = t.one(`INSERT INTO directors (name) VALUES ($1) RETURNING id`, [name])
    const createGenre = t.one(`INSERT INTO genres (type) VALUES ($1) RETURNING id`, [type])

    return t.batch([createPrimContent, createDirector, createGenre])
  })
    .then(newContent => {
      console.log(newContent)
    })
    .catch(error => {
      console.error({
        message: 'Error creating new content!',
        arguments: arguments
      })
      throw error
    })
}

const getAll = () => {
  return db.manyOrNone(`
    SELECT * FROM content
  `)
    .catch(error => {
      console.error({
        message: 'Error getting content!',
        arguments: arguments
      })
      throw error
    })
}

const destroy = (id) => {
  return db.none(`
    DELETE FROM content 
    *
    WHERE id=$1 
  `,
  [id])
  .catch(error => {
    console.error({
      message: 'Error deleting content!',
      arguments: arguments
    })
  })
}
 
module.exports = {
  create,
  getAll,
  destroy
}