DROP TABLE IF EXISTS member;

CREATE TABLE member(
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255),
  role VARCHAR(80),
  status VARCHAR(255),
  date_joined TIMESTAMP(6) WITH TIME ZONE NOT NULL 
);

DROP TABLE TABLE IF EXISTS role;

CREATE TABLE role(
  member_id INTEGER REFERENCES member,
  admin VARCHAR(10) NOT NULL
  standard VARCHAR(10) NOT NULL,
  guest VARCHAR(10) NOT NULL
);

DROP TABLE IF EXISTS director;

CREATE TABLE director(
  id SERIAL PRIMARY KEY,
  name TEXT
);

DROP TABLE IF EXISTS genre;

CREATE TABLE genre(
  id SERIAL PRIMARY KEY,
  type VARCHAR(255)
);

DROP TABLE IF EXISTS rating;

CREATE TABLE rating(
  id SERIAL PRIMARY KEY,
  member_id INTEGER REFERENCES member,
  rating_number INTEGER,
  review TEXT
);

DROP TABLE IF EXISTS content;

CREATE TABLE content(
  id SERIAL PRIMARY KEY,
  name TEXT,
  description TEXT,
  director_id INTEGER REFERENCES director,
  genre_id INTEGER REFERENCES genre,
  image_url TEXT,
  release_year DATE,
  trt INTEGER,
  rating INTEGER REFERENCES rating,
  keyword VARCHAR(255)
);

DROP TABLE IF EXISTS liked_content;

CREATE TABLE liked_content(
  member_id INTEGER REFERENCES member,
  content_id INTEGER REFERENCES member 
);

DROP TABLE IF EXISTS session;

CREATE TABLE "session" (
  "sid" VARCHAR NOT NULL COLLATE "default",
  "sess" json NOT NULL,
  "expire" TIMESTAMP(6)
)
WITH (OIDS=FALSE);
ALTER TABLE "session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;
