const db = require('./db/roles')

module.exports = {
  assignAdminRole: db.assignAdminRole,
  assignStandardRole: db.assignStandardRole,
  removeAdminRole: db.removeAdminRole,
  removeStandardRole: db.removeStandardRole
}