const db = require('./db/members')
const bcrypt = require('bcrypt')

const isValidPassword2 = (password, hashedPassword) => {
  return bcrypt.compare(password, hashedPassword)
}

module.exports = {
  create: db.create,
  findById: db.findById,
  findByEmail: db.findByEmail,
  findByUsername: db.findByUsername,
  getUsername: db.getUsername,
  isValidPassword2,
  setActiveStatus: db.setActiveStatus,
  getAll: db.getAll,
  destroy: db.destroy
}