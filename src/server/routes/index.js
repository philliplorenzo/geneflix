const router = require('express').Router()
const passport = require('passport')
const passportConfig = require('../../authentication')(passport)
const authRoutes = require('./authentication')
const contentRoutes = require('./content')
const memberRoutes = require('./member')
const middlewares = require('../middlewares')
const { renderError } = require('../utils')

router.use(passport.initialize())
router.use(passport.session())

router.get('/', (req, res, next) => {
  res.render('home/home')
})

router.use('/', authRoutes(passportConfig))

router.use('/content', contentRoutes)
router.use('/member', memberRoutes)

router.use(middlewares.sessionChecker)
router.use(middlewares.logErrors)
router.use(middlewares.notFoundHandler)

module.exports = router
