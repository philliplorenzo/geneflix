const express = require('express')
const router = express.Router()
const member = require('../../models/members')

module.exports = passport => {
  router.route('/login')
  .get((req, res) => {
    res.render('auth/login')
  })

  .post(passport.authenticate('local', { successRedirect: '/member',
                                         failureRedirect: '/login',
                                         }))

  router.route('/signup')
  .get((req, res) => {
    res.render('auth/signup')
  })

  .post((req, res) => {
    const { username, email, password } = req.body
      member.create(username, email, password)
      res.redirect('/login')
  })

  router.get('/logout', (request, response) => {
  request.session.destroy(() => {
    response.redirect('/')
  })
})

  return router 
}