const content = require('../../models/content')
const member = require('../../models/members')
const memberHasAccess = require('../../authorization/roles')
const {renderError, renderUnauthorized} = require('../utils')

const router = require('express').Router()

router.route('/create')
  .get((req, res, next) => {
    console.log('do I get a session???', req.session.user)
    member.findById(req.session.user)
    .then((member) => {
      res.render('content/create', { member })
    })
  })

  .post((req, res, next) => {
    const { member } = request
    if (memberHasAccess(member, 'createContent')) {
      content.create(req.body)
        .then(content => {
          if (content) return res.redirect(`/dashboard/${content[0].id}`)
        })
        .catch(error => renderError(error, res)) 
    } else {
      renderUnauthorized(res)
    }
  })

router.route('/listing')
  .get((req, res, next) => {
    content.getAll().then((content) => {
      const id = content.map(content => content.id)
      const name = content.map(content => content.name)
      const description = content.map(content => content.description)
      const trt = content.map(content => content.trt)
      const releaseYear = content.map(content => content.releaseYear)
      const director = content.map(content => content.director)
      const genre = content.map(content => content.genre)
      res.render('content/listing', {
        id: id, 
        name: name, 
        description: description, 
        trt: trt, 
        releaseYear: releaseYear,
        director: director,
        genre: genre
      })
    })
    .catch(error => console.log(error))
  })

  module.exports = router