const router = require('express').Router()
const moment = require('moment')
const member = require('../../models/members')
const roles = require('../../models/roles')
const memberHasAccess = require('../../authorization/roles')

router.get('/:id', (req, res) => {
  console.log('do i get a member here? ', req.session.passport.user)
  member.findById(req.session.passport.user)
  .then(memberId => {res.render('member/profile', {
      member: memberId.username,
      date: moment(memberId.date_joined).format("MMM Do YYYY"),
      role: memberId.role,
      status: memberId.status
    })
  })
})

router.get('/', (req, res) => {
  member.getAll()
    .then( members => res.render('member/dashboard', { members }))
})

router.patch('/setRoles', (req, res) => {
  const id  = Number(req.body.roleLook[0].id)
  const role = req.body.roleLook[0].role
  member.findById(id)
  .then(memberRole => {
    if (memberRole.role === 'admin') {
      console.log('what do I get??', memberRole)
      return roles.assignAdminRole(memberRole.id)
        .then(member.getAll().then(members => res.render('member/dashboard', { members })))
    }
    if (memberRole.role === 'standard'){
      return roles.assignStandardRole(memberRole.id)
        .then(member.getAll().then(members => res.render('member/dashboard', { members })))
    }
    if (memberRole.role === 'guest') {
      return roles.removeStandardRole(memberRole.id)
        .then(member.getAll().then(members => res.render('member/dashboard', { members })))
    }
  })
})


module.exports = router