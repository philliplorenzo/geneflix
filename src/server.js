const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const methodOverride = require('method-override')
const middlewares = require('./server/middlewares') 
const routes = require('./server/routes/')
const config = require('./config').getConfig()

app.set('view engine', 'ejs')
app.set ('views', __dirname + '/views')

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(express.static('public'))
app.use(cookieParser())

app.use(methodOverride('_method'))


let sess = {
  key: 'user_sid',
  store: new (require('connect-pg-simple')(session))(),
  secret: config.session.secret,
  resave: false, 
  saveUninitialized: false, 
  cookie: {
    expires: 10 * 60 * 100000 
  }
}

app.use(session(sess))

app.use('/', routes)

app.use(middlewares.sessionChecker)

app.listen(config.server.port, () => {
  console.log(`listening on http://localhost:${config.server.port}`)
})

module.exports = app

