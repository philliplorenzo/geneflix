const member = require('../models/members')
const roles = require('../models/roles')

const CAPABILITY_ROLES = {
  viewContent: ['admin', 'standard', 'guest', 'superAdmin'],
  viewContentDetails: ['admin', 'standard', 'superAdmin'],
  createContent: ['admin', 'superAdmin'],
  deleteContent: ['admin', 'superAdmin'],
  assignRole: ['superAdmin']
}

const memberHasAccess = (memberId, action) => {
  member.findById(memberId)
    .then(id => {
      const role = memberId.role
      const allActions = Object.keys(CAPABILITY_ROLES)
      const isValidRole = ALL_MEMBER_ROLES.includes(role)

      if (!isValidRole) {
        throw new Error(`member with username: ${member.username} does not have access to this`)
      } else if (!allActions.includes(action)) {
        throw new Error(`Tried to get permissions for an invalid action`)
      } else {
        const capabilities = CAPABILITY_ROLES.includes(action)
        return capabilities.includes(member.role)
      }
  })
  
}

module.exports = memberHasAccess