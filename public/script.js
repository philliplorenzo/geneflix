document.addEventListener('DOMContentLoaded', function (event) {
  console.log("DOM fully loaded and parsed")

  $(".slideshow > div:gt(0)").hide()

  setInterval(function() {
    $('.slideshow > div:first')
      .fadeOut(1000)
      .next()
      .fadeIn(1000)
      .end()
      .appendTo('.slideshow')
  }, 3000)

  $('.dropdown-button').dropdown({
    inDuration: 300,
    outDuration: 225,
    constrainWidth: false, // Does not change width of dropdown to that of the activator
    hover: true, // Activate on hover
    gutter: 0, // Spacing from edge
    belowOrigin: false, // Displays dropdown below the button
    alignment: 'left', // Displays dropdown with edge aligned to the left of button
    stopPropagation: false // Stops event propagation
  });

  const checkStatus = function(response) {
    if (response.status === 200) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(new Error(response.statusText))
    }
  }
  
  $('.dropdown-button').on("click", function(e) {
    let roleLook = []
    e.preventDefault()
    console.log($(e.target).data("role"))
    let id = $(e.target).data("id")
    console.log('id please', id)
    let role = $(e.target).data("role")
    console.log('what is your latest role? ', role)
    roleLook.push({ id, role })
    console.log('can I get a look? ', roleLook)
      fetch('http://localhost:3000/member/setRoles', {
        method: 'PATCH',
        mode: 'same-origin',
        redirect: '/',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify({
            roleLook
          })
      }).then(checkStatus)
      .catch(error => console.log(error))
  })
  
  // $(".list-roles").each(function(el) {
  //   console.log('what do i get??', el)
  //   let id = $(el).data( "id" )
  //   console.log('id please', id)
  //   let role = $(el).data( "role" )
  //   console.log('what is your latest role? ', role)
  // })


})