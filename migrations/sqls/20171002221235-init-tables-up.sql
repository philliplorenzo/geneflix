CREATE TABLE member(
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role VARCHAR(80), 
  status BOOLEAN,
  date_joined TIMESTAMP(6) DEFAULT now() 
);

CREATE TABLE director(
  id SERIAL PRIMARY KEY,
  name TEXT
);

CREATE TABLE genre(
  id SERIAL PRIMARY KEY,
  type VARCHAR(255)
);

CREATE TABLE rating(
  id SERIAL PRIMARY KEY,
  member_id INTEGER REFERENCES member,
  rating_number INTEGER,
  review TEXT
);

CREATE TABLE content(
  id SERIAL PRIMARY KEY,
  name TEXT,
  description TEXT,
  director_id INTEGER REFERENCES director,
  genre_id INTEGER REFERENCES genre,
  image_url TEXT,
  release_year DATE,
  trt INTEGER,
  rating INTEGER REFERENCES rating,
  keyword VARCHAR(255)
);

CREATE TABLE liked_content(
  member_id INTEGER REFERENCES member,
  content_id INTEGER REFERENCES member 
);

CREATE TABLE "session" (
  "sid" VARCHAR NOT NULL COLLATE "default",
  "sess" json NOT NULL,
  "expire" TIMESTAMP(6)
)
WITH (OIDS=FALSE);
ALTER TABLE "session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;